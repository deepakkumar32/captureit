//
//  VideoView.swift
//  CaptureIT
//
//  Created by Deepak jaswal on 21/09/20.
//  Copyright © 2020 Deepak jaswal. All rights reserved.
//

import UIKit
import AVFoundation


var defaultSize = CGSize(width: 1920, height: 1080)

public protocol VideoPickerDelegate: class {
    func didSelect(url: URL?)
}

open class VideoView: UIView {
    
    public var filters: String = "" {
        didSet {
            guard self.url != nil else { return }
            self.setup(url: self.url!)
        }
    }
    
    var saturation: Int = 0 {
        didSet {
            guard self.url != nil else { return }
            
            let key = kCIInputSaturationKey
            
            manageAdjustmentKeys(key, saturation)
        }
    }
    
    var brightness: Int = 0 {
        didSet {
            guard self.url != nil else { return }
            let key = kCIInputBrightnessKey
            manageAdjustmentKeys(key, brightness)
        }
    }
    
    var contrast: Int = 0 {
        didSet {
            guard self.url != nil else { return }
            
            let key = kCIInputContrastKey
            manageAdjustmentKeys(key, contrast)
        }
    }
    
    var warm: Int = 0 {
        didSet {
            guard self.url != nil else { return }
            
            manageWarmForAdjustmentKey(warm)
        }
    }
    var cold: Int = 0 {
        didSet {
            guard self.url != nil else { return }
            
            manageColdForAdjustmentKey(cold)
        }
    }
    
    var noiseReduct: Int = 0 {
        didSet {
            guard self.url != nil else { return }
            
            manageNoiseReductForAdjustmentKey(noiseReduct)
        }
    }
    
    var vignette: Int = 0 {
        didSet {
            guard self.url != nil else { return }
            
            manageVignetteForAdjustmentKey(vignette)
        }
    }
    var sharpness: Int = 0 {
        didSet {
            guard self.url != nil else { return }
            
            manageSharpnessForAdjustmentKey(sharpness)
        }
    }
    
    var exposure: Int = 0 {
        didSet {
            guard self.url != nil else { return }
            
            manageExposureForAdjustmentKey(exposure)
        }
    }
    
    var hightlight: Int = 0 {
        didSet {
            guard self.url != nil else { return }
            
            manageHighlightForAdjustmentKey(hightlight)
        }
    }
    
    public enum Repeat {
        case once
        case loop
    }
    
    open override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    private var playerLayer: AVPlayerLayer {
        return self.layer as! AVPlayerLayer
    }
    
    public var player: AVPlayer? {
        get {
            self.playerLayer.player
        }
        set {
            self.playerLayer.player = newValue
        }
    }
    
    
    open override var contentMode: UIView.ContentMode {
        didSet {
            switch self.contentMode {
            case .scaleAspectFit:
                self.playerLayer.videoGravity = .resizeAspect
            case .scaleAspectFill:
                self.playerLayer.videoGravity = .resizeAspectFill
            default:
                self.playerLayer.videoGravity = .resize
            }
        }
    }
    
    public var `repeat`: Repeat = .once
    
    public var url: URL? {
        didSet {
            guard let url = self.url else {
                self.teardown()
                return
            }
            self.setup(url: url)
        }
    }
    
    @available(*, unavailable)
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    @available(*, unavailable)
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    public init() {
        super.init(frame: .zero)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.initialize()
    }
    
    open func initialize() {
        
    }
    
    deinit {
        self.teardown()
    }
    
    
    private func setup(url: URL) {
        
        if self.filters != "" {
            
            let filter = CIFilter(name: self.filters)!// CIGaussianBlur // CIPhotoEffectChrome // CIPhotoEffectTonal
            let asset = AVAsset(url: url)
            let item = AVPlayerItem(asset: asset)
            
            //            //Create Directory path for Save
            //            let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            //            var outputURL = documentDirectory.appendingPathComponent("EffectVideo")
            //            do {
            //                try FileManager.default.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
            //                outputURL = outputURL.appendingPathComponent("\(outputURL.lastPathComponent).m4v")
            //            }catch let error {
            ////                failure(error.localizedDescription)
            //                print(error.localizedDescription)
            //            }
            //
            //            //Remove existing file
            //            self.deleteFile(outputURL)
            
            item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { request in
                
                // Clamp to avoid blurring transparent pixels at the image edges
                let source = request.sourceImage.clampedToExtent()
                filter.setValue(source, forKey: kCIInputImageKey)
                
                // Vary filter parameters based on video timing
                //     let seconds = CMTimeGetSeconds(request.compositionTime)
                // filter.setValue(seconds * 10.0, forKey: kCIInputRadiusKey)
                
                // Crop the blurred output to the bounds of the original image
                let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
                
                // Provide the filter output to the composition
                request.finish(with: output, context: nil)
            })
            
            self.player = AVPlayer(playerItem: item)
            
            //export the video to as per your requirement conversion
            //            guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetMediumQuality) else { return }
            //            exportSession.outputFileType = AVFileType.mov
            //            exportSession.outputURL = outputURL
            //            exportSession.videoComposition = item.videoComposition
            //
            //            exportSession.exportAsynchronously(completionHandler: {
            //                switch exportSession.status {
            //                case .completed:
            //                  //  success(outputURL)
            //                    print(outputURL)
            //
            //                case .failed:
            //                  //  failure(exportSession.error?.localizedDescription)
            //                    print(exportSession.error?.localizedDescription)
            //
            //                case .cancelled:
            //                  //  failure(exportSession.error?.localizedDescription)
            //                    print(exportSession.error?.localizedDescription)
            //
            //                default:
            //                  //  failure(exportSession.error?.localizedDescription)
            //                    print(exportSession.error?.localizedDescription)
            //                }
            //            })
            
        } else {
            self.player = AVPlayer(playerItem: AVPlayerItem(url: url))
        }
        
        self.player?.currentItem?.addObserver(self,
                                              forKeyPath: "status",
                                              options: [.old, .new],
                                              context: nil)
        
        self.player?.addObserver(self, forKeyPath: "rate", options: [.old, .new], context: nil)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.itemDidPlayToEndTime(_:)),
                                               name: .AVPlayerItemDidPlayToEndTime,
                                               object: self.player?.currentItem)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.itemFailedToPlayToEndTime(_:)),
                                               name: .AVPlayerItemFailedToPlayToEndTime,
                                               object: self.player?.currentItem)
    }
    
    //MARK: Add filter to video
    func addfiltertoVideo(strfiltername : String, success: @escaping ((URL) -> Void), failure: @escaping ((String?) -> Void)) {
        
        //FilterName
        let filter = CIFilter(name:strfiltername)
        
        //Asset
        let asset = AVAsset(url: self.url!)
        
        //Create Directory path for Save
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        var outputURL = documentDirectory.appendingPathComponent("EffectVideo")
        do {
            try FileManager.default.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
            outputURL = outputURL.appendingPathComponent("\(outputURL.lastPathComponent).m4v")
        }catch let error {
            failure(error.localizedDescription)
        }
        
        //Remove existing file
        self.deleteFile(outputURL)
        
        //AVVideoComposition
        let composition = AVVideoComposition(asset: asset, applyingCIFiltersWithHandler: { request in
            
            // Clamp to avoid blurring transparent pixels at the image edges
            let source = request.sourceImage.clampedToExtent()
            filter?.setValue(source, forKey: kCIInputImageKey)
            
            // Crop the blurred output to the bounds of the original image
            let output = filter?.outputImage!.cropped(to: request.sourceImage.extent)
            
            // Provide the filter output to the composition
            request.finish(with: output!, context: nil)
            
        })
        
        //export the video to as per your requirement conversion
        guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else { return }
        exportSession.outputFileType = AVFileType.mov
        exportSession.outputURL = outputURL
        exportSession.videoComposition = composition
        
        exportSession.exportAsynchronously(completionHandler: {
            switch exportSession.status {
            case .completed:
                success(outputURL)
                
            case .failed:
                failure(exportSession.error?.localizedDescription)
                
            case .cancelled:
                failure(exportSession.error?.localizedDescription)
                
            default:
                failure(exportSession.error?.localizedDescription)
            }
        })
    }
    
    func videoScaleAssetSpeed(fromURL url: URL,  by scale: Float64, success: @escaping ((URL) -> Void), failure: @escaping ((String?) -> Void)) {
        
        /// Asset
        let asset = AVPlayerItem(url: url).asset
        
        // Composition Audio Video
        let mixComposition = AVMutableComposition()
        
        //TotalTimeRange
        let timeRange = CMTimeRangeMake(start: CMTime.zero, duration: asset.duration)
        
        /// Video Tracks
        let videoTracks = asset.tracks(withMediaType: AVMediaType.video)
        if videoTracks.count == 0 {
            /// Can not find any video track
            return
        }
        
        /// Video track
        let videoTrack = videoTracks.first!
        
        let compositionVideoTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        /// Audio Tracks
        let audioTracks = asset.tracks(withMediaType: AVMediaType.audio)
        if audioTracks.count > 0 {
            /// Use audio if video contains the audio track
            let compositionAudioTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)
            
            /// Audio track
            let audioTrack = audioTracks.first!
            do {
                try compositionAudioTrack?.insertTimeRange(timeRange, of: audioTrack, at: CMTime.zero)
                let destinationTimeRange = CMTimeMultiplyByFloat64(asset.duration, multiplier:(1/scale))
                compositionAudioTrack?.scaleTimeRange(timeRange, toDuration: destinationTimeRange)
                
                compositionAudioTrack?.preferredTransform = audioTrack.preferredTransform
                
            } catch _ {
                /// Ignore audio error
            }
        }
        
        do {
            try compositionVideoTrack?.insertTimeRange(timeRange, of: videoTrack, at: CMTime.zero)
            let destinationTimeRange = CMTimeMultiplyByFloat64(asset.duration, multiplier:(1/scale))
            compositionVideoTrack?.scaleTimeRange(timeRange, toDuration: destinationTimeRange)
            
            /// Keep original transformation
            compositionVideoTrack?.preferredTransform = videoTrack.preferredTransform
            
            //Create Directory path for Save
            let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            var outputURL = documentDirectory.appendingPathComponent("SpeedVideo")
            do {
                try FileManager.default.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
                outputURL = outputURL.appendingPathComponent("\(outputURL.lastPathComponent).m4v")
            }catch let error {
                failure(error.localizedDescription)
            }
            
            //Remove existing file
            self.deleteFile(outputURL)
            
            //export the video to as per your requirement conversion
            if let exportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality) {
                exportSession.outputURL = outputURL
                exportSession.outputFileType = AVFileType.mp4
                exportSession.shouldOptimizeForNetworkUse = true
                
                /// try to export the file and handle the status cases
                exportSession.exportAsynchronously(completionHandler: {
                    switch exportSession.status {
                    case .completed :
                        success(outputURL)
                    case .failed:
                        if let _error = exportSession.error?.localizedDescription {
                            failure(_error)
                        }
                    case .cancelled:
                        if let _error = exportSession.error?.localizedDescription {
                            failure(_error)
                        }
                    default:
                        if let _error = exportSession.error?.localizedDescription {
                            failure(_error)
                        }
                    }
                })
            } else {
                failure(nil)
            }
        } catch {
            // Handle the error
            failure("Inserting time range failed.")
        }
        
    }
    
    func deleteFile(_ filePath:URL) {
        guard FileManager.default.fileExists(atPath: filePath.path) else {
            return
        }
        do {
            try FileManager.default.removeItem(atPath: filePath.path)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
    
    private func teardown() {
        self.player?.pause()
        
        self.player?.currentItem?.removeObserver(self, forKeyPath: "status")
        
        self.player?.removeObserver(self, forKeyPath: "rate")
        
        NotificationCenter.default.removeObserver(self,
                                                  name: .AVPlayerItemDidPlayToEndTime,
                                                  object: self.player?.currentItem)
        
        NotificationCenter.default.removeObserver(self,
                                                  name: .AVPlayerItemFailedToPlayToEndTime,
                                                  object: self.player?.currentItem)
        
        self.player = nil
    }
    
    
    @objc func itemDidPlayToEndTime(_ notification: NSNotification) {
        guard self.repeat == .loop else {
            return
        }
        self.player?.seek(to: .zero)
        self.player?.play()
    }
    
    @objc func itemFailedToPlayToEndTime(_ notification: NSNotification) {
        self.teardown()
    }
    
    
    open override func observeValue(forKeyPath keyPath: String?,
                                    of object: Any?,
                                    change: [NSKeyValueChangeKey : Any]?,
                                    context: UnsafeMutableRawPointer?) {
        if keyPath == "status", let status = self.player?.currentItem?.status, status == .failed {
            self.teardown()
        }
        
        if
            keyPath == "rate",
            let player = self.player,
            player.rate == 0,
            let item = player.currentItem,
            !item.isPlaybackBufferEmpty,
            CMTimeGetSeconds(item.duration) != CMTimeGetSeconds(player.currentTime())
        {
            self.player?.play()
        }
    }
    
    func mergeTwoVideosArry(arrayVideos:[AVAsset], success: @escaping ((URL) -> Void), failure: @escaping ((String?) -> Void)) {
        
        let mainComposition = AVMutableComposition()
        let compositionVideoTrack = mainComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        compositionVideoTrack?.preferredTransform = CGAffineTransform(rotationAngle: .pi / 3)
        
        let soundtrackTrack = mainComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        var insertTime = CMTime.zero
        
        for videoAsset in arrayVideos {
            try! compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: .video)[0], at: insertTime)
            try! soundtrackTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: .audio)[0], at: insertTime)
            
            insertTime = CMTimeAdd(insertTime, videoAsset.duration)
        }
        
        //Create Directory path for Save
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        var outputURL = documentDirectory.appendingPathComponent("MergeTwoVideos")
        do {
            try FileManager.default.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
            outputURL = outputURL.appendingPathComponent("\(outputURL.lastPathComponent).m4v")
        }catch let error {
            failure(error.localizedDescription)
        }
        
        //Remove existing file
        self.deleteFile(outputURL)
        
        //export the video to as per your requirement conversion
        if let exportSession = AVAssetExportSession(asset: mainComposition, presetName: AVAssetExportPresetHighestQuality) {
            exportSession.outputURL = outputURL
            exportSession.outputFileType = AVFileType.mp4
            exportSession.shouldOptimizeForNetworkUse = true
            
            /// try to export the file and handle the status cases
            exportSession.exportAsynchronously(completionHandler: {
                switch exportSession.status {
                case .completed :
                    success(outputURL)
                case .failed:
                    if let _error = exportSession.error?.localizedDescription {
                        failure(_error)
                    }
                case .cancelled:
                    if let _error = exportSession.error?.localizedDescription {
                        failure(_error)
                    }
                default:
                    if let _error = exportSession.error?.localizedDescription {
                        failure(_error)
                    }
                }
            })
        } else {
            failure("video export session failed")
        }
    }
    
    func transitionAnimation(videoUrl: URL, animation:Bool, type: Int, playerSize: CGRect,success: @escaping ((URL) -> Void), failure: @escaping ((String?) -> Void)) {
        var insertTime = CMTime.zero
        var arrayLayerInstructions:[AVMutableVideoCompositionLayerInstruction] = []
        var outputSize = CGSize(width: 0, height: 0)
        
        let aVideoAsset = AVAsset(url: videoUrl)
        
        // Determine video output size
        
        let videoTrack = aVideoAsset.tracks(withMediaType: AVMediaType.video)[0]
        
        let assetInfo = self.orientationFromTransform(videoTrack.preferredTransform)
        
        var videoSize = videoTrack.naturalSize
        if assetInfo.isPortrait == true {
            videoSize.width = videoTrack.naturalSize.height
            videoSize.height = videoTrack.naturalSize.width
        }
        
        if videoSize.height > outputSize.height {
            outputSize = videoSize
        }
        
        
        if outputSize.width == 0 || outputSize.height == 0 {
            outputSize = defaultSize
        }
        
        // Init composition
        let mixComposition = AVMutableComposition()
        
        // Get video track
        guard let videoTrackk = aVideoAsset.tracks(withMediaType: AVMediaType.video).first else {
            return
        }
        
        // Get audio track
        var audioTrack:AVAssetTrack?
        //  if videoAsset.tracks(withMediaType: AVMediaType.audio).count > 0 {
        audioTrack = aVideoAsset.tracks(withMediaType: AVMediaType.audio).first
        /* }
         else {
         audioTrack = silenceSoundTrack
         }*/
        
        // Init video & audio composition track
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        
        let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        
        do {
            let startTime = CMTime.zero
            let duration = aVideoAsset.duration
            
            // Add video track to video composition at specific time
            try videoCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: startTime, duration: duration),
                                                       of: videoTrackk,
                                                       at: insertTime)
            
            // Add audio track to audio composition at specific time
            if let audioTrack = audioTrack {
                try audioCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: startTime, duration: duration), of: audioTrack, at: insertTime)
            }
            
            // Add instruction for video track
            let layerInstruction = self.videoCompositionInstructionForTrackWithSizeandTime(track: videoCompositionTrack!, asset: aVideoAsset, standardSize: outputSize, atTime: insertTime)
            
            // Hide video track before changing to new track
            let endTime = CMTimeAdd(insertTime, duration)
            
            //if animation {
            let timeScale = aVideoAsset.duration.timescale
            let durationAnimation = CMTime.init(seconds: 1, preferredTimescale: timeScale)
            
            // layerInstruction.setOpacityRamp(fromStartOpacity: 1.0, toEndOpacity: 0.0, timeRange: CMTimeRange.init(start: endTime, duration: durationAnimation))
            switch type {
            case 0:
                layerInstruction.setTransformRamp(fromStart: CGAffineTransform(translationX: 500, y: 0), toEnd: CGAffineTransform(translationX: 0, y: 0), timeRange: CMTimeRange.init(start: CMTime.zero, duration: durationAnimation))
            case 1:
                layerInstruction.setTransformRamp(fromStart: CGAffineTransform(translationX: -500, y: 0), toEnd: CGAffineTransform(translationX: 0, y: 0), timeRange: CMTimeRange.init(start: CMTime.zero, duration: durationAnimation))
            case 2:
                layerInstruction.setTransformRamp(fromStart: CGAffineTransform(translationX: 0, y: -600), toEnd: CGAffineTransform(translationX: 0, y: 0), timeRange: CMTimeRange.init(start: CMTime.zero, duration: durationAnimation))
            case 3:
                layerInstruction.setTransformRamp(fromStart: CGAffineTransform(translationX: 0, y: 600), toEnd: CGAffineTransform(translationX: 0, y: 0), timeRange: CMTimeRange.init(start: CMTime.zero, duration: durationAnimation))
            case 4:
                layerInstruction.setTransformRamp(fromStart: CGAffineTransform(translationX: -600, y: -600), toEnd: CGAffineTransform(translationX: 0, y: 0), timeRange: CMTimeRange.init(start: CMTime.zero, duration: durationAnimation))
            case 5:
                layerInstruction.setTransformRamp(fromStart: CGAffineTransform(translationX: 600, y: 600), toEnd: CGAffineTransform(translationX: 0, y: 0), timeRange: CMTimeRange.init(start: CMTime.zero, duration: durationAnimation))
            case 6:
                layerInstruction.setOpacityRamp(fromStartOpacity: 0.0, toEndOpacity: 1.0, timeRange: CMTimeRange.init(start: CMTime.zero, duration: durationAnimation))
            default:
                break
            }
            layerInstruction.setOpacity(1, at: endTime)
            arrayLayerInstructions.append(layerInstruction)
            insertTime = CMTimeAdd(insertTime, duration)
        }
        catch {
            failure(error.localizedDescription)
        }
        
        
        // Main video composition instruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: insertTime)
        mainInstruction.layerInstructions = arrayLayerInstructions
        
        // Main video composition
        let mainComposition = AVMutableVideoComposition()
        mainComposition.instructions = [mainInstruction]
        mainComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        mainComposition.renderSize = outputSize
        
        //Create Directory path for Save
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        var outputURL = documentDirectory.appendingPathComponent("TransitionVideo")
        do {
            try FileManager.default.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
            outputURL = outputURL.appendingPathComponent("\(outputURL.lastPathComponent).m4v")
        }catch let error {
            failure(error.localizedDescription)
        }
        
        //Remove existing file
        self.deleteFile(outputURL)
        
        //export the video to as per your requirement conversion
        if let exportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality) {
            exportSession.outputURL = outputURL
            exportSession.outputFileType = AVFileType.mp4
            exportSession.shouldOptimizeForNetworkUse = true
            exportSession.videoComposition = mainComposition
            /// try to export the file and handle the status cases
            exportSession.exportAsynchronously(completionHandler: {
                switch exportSession.status {
                case .completed :
                    success(outputURL)
                case .failed:
                    if let _error = exportSession.error?.localizedDescription {
                        failure(_error)
                    }
                case .cancelled:
                    if let _error = exportSession.error?.localizedDescription {
                        failure(_error)
                    }
                default:
                    if let _error = exportSession.error?.localizedDescription {
                        failure(_error)
                    }
                }
            })
        } else {
            failure("video export session failed")
        }
    }
    
    func orientationFromTransform(_ transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
            assetOrientation = .right
            isPortrait = true
        } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
            assetOrientation = .left
            isPortrait = true
        } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
            assetOrientation = .up
        } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
            assetOrientation = .down
        }
        return (assetOrientation, isPortrait)
    }
    
    func videoCompositionInstructionForTrackWithSizeandTime(track: AVCompositionTrack, asset: AVAsset, standardSize:CGSize, atTime: CMTime) -> AVMutableVideoCompositionLayerInstruction {
        
        let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        
        let transform = assetTrack.preferredTransform
        let assetInfo = orientationFromTransform(transform)
        
        var aspectFillRatio:CGFloat = 1
        if assetTrack.naturalSize.height < assetTrack.naturalSize.width {
            aspectFillRatio = standardSize.height / assetTrack.naturalSize.height
        }
        else {
            aspectFillRatio = standardSize.width / assetTrack.naturalSize.width
        }
        
        if assetInfo.isPortrait {
            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
            let posX = standardSize.width/2 - (assetTrack.naturalSize.height * aspectFillRatio)/2
            let posY = standardSize.height/2 - (assetTrack.naturalSize.width * aspectFillRatio)/2
            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
            instruction.setTransform(assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor), at: atTime)
            
        } else {
            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
            
            let posX = standardSize.width/2 - (assetTrack.naturalSize.width * aspectFillRatio)/2
            let posY = standardSize.height/2 - (assetTrack.naturalSize.height * aspectFillRatio)/2
            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
            
            var concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor)
            
            if assetInfo.orientation == .down {
                let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                concat = fixUpsideDown.concatenating(scaleFactor).concatenating(moveFactor)
            }
            instruction.setTransform(concat, at: atTime)
        }
        return instruction
    }
    
    //MARK: crop the Audio which you select portion
    func trimAudio(sourceURL: URL, startTime: Double, stopTime: Double, success: @escaping ((URL) -> Void), failure: @escaping ((String?) -> Void)) {
        /// Asset
        let asset = AVAsset(url: sourceURL)
        //        let length = Float(asset.duration.value) / Float(asset.duration.timescale)
        //        print("video length: \(length) seconds")
        
        let compatiblePresets = AVAssetExportSession.exportPresets(compatibleWith:asset)
        
        if compatiblePresets.contains(AVAssetExportPresetMediumQuality) {
            
            //Create Directory path for Save
            let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            var outputURL = documentDirectory.appendingPathComponent("TrimAudio")
            do {
                try FileManager.default.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
                outputURL = outputURL.appendingPathComponent("\(sourceURL.lastPathComponent).m4a")
            }catch let error {
                failure(error.localizedDescription)
            }
            
            //Remove existing file
            self.deleteFile(outputURL)
            
            //export the audio to as per your requirement conversion
            guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A) else{return}
            exportSession.outputURL = outputURL
            exportSession.outputFileType = AVFileType.m4a
            
            let range: CMTimeRange = CMTimeRangeFromTimeToTime(start: CMTimeMakeWithSeconds(startTime, preferredTimescale: asset.duration.timescale), end: CMTimeMakeWithSeconds(stopTime, preferredTimescale: asset.duration.timescale))
            exportSession.timeRange = range
            
            exportSession.exportAsynchronously(completionHandler: {
                switch exportSession.status {
                case .completed:
                    success(outputURL)
                    
                case .failed:
                    if let _error = exportSession.error?.localizedDescription {
                        failure(_error)
                    }
                    
                case .cancelled:
                    if let _error = exportSession.error?.localizedDescription {
                        failure(_error)
                    }
                    
                default:
                    if let _error = exportSession.error?.localizedDescription {
                        failure(_error)
                    }
                }
            })
        }
    }
    
    func mergeVideoWithAudio(videoUrl: URL, audioUrl: URL, success: @escaping ((URL) -> Void), failure: @escaping ((String?) -> Void)) {
        
        //Audio & Video Asset
        let aVideoAsset: AVAsset = AVAsset(url: videoUrl)
        let aAudioAsset: AVAsset = AVAsset(url: audioUrl)
        
        //Audio video track Mix composition
        let mixComposition: AVMutableComposition = AVMutableComposition()
        var mutableCompositionVideoTrack: [AVMutableCompositionTrack] = []
        var mutableCompositionAudioTrack: [AVMutableCompositionTrack] = []
        let totalVideoCompositionInstruction : AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
        
        if let videoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid), let audioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid) {
            mutableCompositionVideoTrack.append(videoTrack)
            mutableCompositionAudioTrack.append(audioTrack)
            
            if let aVideoAssetTrack: AVAssetTrack = aVideoAsset.tracks(withMediaType: .video).first, let aAudioAssetTrack: AVAssetTrack = aAudioAsset.tracks(withMediaType: .audio).first {
                do {
                    try mutableCompositionVideoTrack.first?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aVideoAssetTrack, at: CMTime.zero)
                    try mutableCompositionAudioTrack.first?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: CMTime.zero)
                    videoTrack.preferredTransform = aVideoAssetTrack.preferredTransform
                    
                } catch{
                    failure(error.localizedDescription)
                }
                
                totalVideoCompositionInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero,duration: aVideoAssetTrack.timeRange.duration)
            }
        }
        
        let mutableVideoComposition: AVMutableVideoComposition = AVMutableVideoComposition()
        mutableVideoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        mutableVideoComposition.renderSize = CGSize(width: 1920, height: 1080) //(720, 480), (1920,1080)
        
        //Create Directory path for Save
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        var outputURL = documentDirectory.appendingPathComponent("MergeVideowithAudio")
        do {
            try FileManager.default.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
            outputURL = outputURL.appendingPathComponent("\(outputURL.lastPathComponent).m4v")
        }catch let error {
            failure(error.localizedDescription)
        }
        
        //Remove existing file
        self.deleteFile(outputURL)
        
        //export the video to as per your requirement conversion
        if let exportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality) {
            exportSession.outputURL = outputURL
            exportSession.outputFileType = AVFileType.mp4
            exportSession.shouldOptimizeForNetworkUse = true
            
            /// try to export the file and handle the status cases
            exportSession.exportAsynchronously(completionHandler: {
                switch exportSession.status {
                case .completed :
                    success(outputURL)
                case .failed:
                    if let _error = exportSession.error?.localizedDescription {
                        failure(_error)
                    }
                case .cancelled:
                    if let _error = exportSession.error?.localizedDescription {
                        failure(_error)
                    }
                default:
                    if let _error = exportSession.error?.localizedDescription {
                        failure(_error)
                    }
                }
            })
        } else {
            failure("video export session failed")
        }
    }
    
    public func manageAdjustmentKeys(_ key: String, _ value: Int) {
        
        /*  guard let filter = CIFilter(name: "CIHighlightShadowAdjust") else { return }
         //           filter.setValue(ciImage, forKey: kCIInputImageKey)
         filter.setValue(value, forKey: "inputHighlightAmount")
         filter.setValue(value+2, forKey: "inputShadowAmount")*/
        
        let filter = CIFilter(name: "CIColorControls")!
        filter.setValue(value, forKey: key)
        let asset = AVAsset(url: self.url!)
        let item = AVPlayerItem(asset: asset)
        
        item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { request in
            
            let source = request.sourceImage.clampedToExtent()
            filter.setValue(source, forKey: kCIInputImageKey)
            
            let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
            
            request.finish(with: output, context: nil)
        })
        
        self.player = AVPlayer(playerItem: item)
    }
    
    public func manageWarmForAdjustmentKey(_ value: Int) {
        
        let filter = CIFilter(name: "CITemperatureAndTint")!
        
        let warmX = (value) + 65000
        let warmy = (5 + value) + 500 // Was never used
        
        filter.setValue(CIVector(x: CGFloat(warmX), y: 0), forKey: "inputNeutral")
        filter.setValue(CIVector(x: 65000, y: 0), forKey: "inputTargetNeutral")
        print("warm:\(warmX)\(warmy)")
        
        //        filter.setValue(CIVector(x: 16000, y: 1000), forKey: "inputNeutral")
        //        filter.setValue(CIVector(x: 1000, y: 500), forKey: "inputTargetNeutral")
        
        let asset = AVAsset(url: self.url!)
        let item = AVPlayerItem(asset: asset)
        
        item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { request in
            
            let source = request.sourceImage.clampedToExtent()
            filter.setValue(source, forKey: kCIInputImageKey)
            
            let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
            
            request.finish(with: output, context: nil)
        })
        
        self.player = AVPlayer(playerItem: item)
    }
    public func manageColdForAdjustmentKey(_ value: Int) {
        
        let filter = CIFilter(name: "CITemperatureAndTint")!
        
        let warmX = (value) + 1600
        let warmy = (5 + value) + 380 // Was never used
        
        filter.setValue(CIVector(x: CGFloat(warmX), y: 0), forKey: "inputNeutral")
        filter.setValue(CIVector(x: 1600, y: 0), forKey: "inputTargetNeutral")
        print("warm:\(warmX)\(warmy)")
        
        //        filter.setValue(CIVector(x: 16000, y: 1000), forKey: "inputNeutral")
        //        filter.setValue(CIVector(x: 1000, y: 500), forKey: "inputTargetNeutral")
        
        let asset = AVAsset(url: self.url!)
        let item = AVPlayerItem(asset: asset)
        
        item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { request in
            
            let source = request.sourceImage.clampedToExtent()
            filter.setValue(source, forKey: kCIInputImageKey)
            
            let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
            
            request.finish(with: output, context: nil)
        })
        
        self.player = AVPlayer(playerItem: item)
    }
    
    public func manageNoiseReductForAdjustmentKey(_ value: Int) {
        
        let filter = CIFilter(name: "CINoiseReduction")!
        
        let warmX = (value) + Int(0.2)
        let warmy = (Int(0.40) + value) // Was never used
        
        filter.setValue(Float(value) * (0.2), forKey: "inputNoiseLevel")
        filter.setValue(Float(value) * (0.40), forKey: "inputSharpness")
        //        filter.setValue(CIVector(x: CGFloat(warmX), y: 0), forKey: "inputNoiseLevel")
        //        filter.setValue(CIVector(x: 1600, y: 0), forKey: "inputSharpness")
        print("warm:\(warmX)\(warmy)")
        
        //        filter.setValue(CIVector(x: 16000, y: 1000), forKey: "inputNeutral")
        //        filter.setValue(CIVector(x: 1000, y: 500), forKey: "inputTargetNeutral")
        
        let asset = AVAsset(url: self.url!)
        let item = AVPlayerItem(asset: asset)
        
        item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { request in
            
            let source = request.sourceImage.clampedToExtent()
            filter.setValue(source, forKey: kCIInputImageKey)
            
            let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
            
            request.finish(with: output, context: nil)
        })
        
        self.player = AVPlayer(playerItem: item)
    }
    
    public func manageVignetteForAdjustmentKey(_ value: Int) {
        
        let filter = CIFilter(name: "CIVignette")!
        
        filter.setValue(value, forKey: "inputIntensity")
        filter.setValue(5, forKey: "inputRadius")
        //        filter.setValue(CIVector(x: CGFloat(warmX), y: 0), forKey: "inputNoiseLevel")
        //        filter.setValue(CIVector(x: 1600, y: 0), forKey: "inputSharpness")
        
        //        filter.setValue(CIVector(x: 16000, y: 1000), forKey: "inputNeutral")
        //        filter.setValue(CIVector(x: 1000, y: 500), forKey: "inputTargetNeutral")
        
        let asset = AVAsset(url: self.url!)
        let item = AVPlayerItem(asset: asset)
        
        item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { request in
            
            let source = request.sourceImage.clampedToExtent()
            filter.setValue(source, forKey: kCIInputImageKey)
            
            let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
            
            request.finish(with: output, context: nil)
        })
        
        self.player = AVPlayer(playerItem: item)
    }
    
    public func manageSharpnessForAdjustmentKey(_ value: Int) {
        
        let filter = CIFilter(name: "CIUnsharpMask")!
        
        filter.setValue(value, forKey: "inputIntensity")
        filter.setValue(1.0, forKey: "inputRadius")
        
        let asset = AVAsset(url: self.url!)
        let item = AVPlayerItem(asset: asset)
        
        item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { request in
            
            let source = request.sourceImage.clampedToExtent()
            filter.setValue(source, forKey: kCIInputImageKey)
            
            let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
            
            request.finish(with: output, context: nil)
        })
        
        self.player = AVPlayer(playerItem: item)
    }
    
    public func manageExposureForAdjustmentKey(_ value: Int) {
        
        let filter = CIFilter(name: "CIExposureAdjust")!
        
        filter.setValue(value, forKey: "inputEV")
        
        let asset = AVAsset(url: self.url!)
        let item = AVPlayerItem(asset: asset)
        
        item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { request in
            
            let source = request.sourceImage.clampedToExtent()
            filter.setValue(source, forKey: kCIInputImageKey)
            
            let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
            
            request.finish(with: output, context: nil)
        })
        
        self.player = AVPlayer(playerItem: item)
    }
    
    public func manageHighlightForAdjustmentKey(_ value: Int) {
        
        let filter = CIFilter(name: "CIHighlightShadowAdjust")!
        
        filter.setValue(value, forKey: "inputHighlightAmount")
        filter.setValue(value * 3, forKey: "inputShadowAmount")
        
        let asset = AVAsset(url: self.url!)
        let item = AVPlayerItem(asset: asset)
        
        item.videoComposition = AVVideoComposition(asset: asset,  applyingCIFiltersWithHandler: { request in
            
            let source = request.sourceImage.clampedToExtent()
            filter.setValue(source, forKey: kCIInputImageKey)
            
            let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
            
            request.finish(with: output, context: nil)
        })
        
        self.player = AVPlayer(playerItem: item)
    }
}
