//
//  Extensions.swift
//  CaptureIT
//
//  Created by Deepak jaswal on 28/09/20.
//  Copyright © 2020 Deepak jaswal. All rights reserved.
//

import Foundation

//MARK: String for FileName and FileExtension
extension String {
    
    func fileName() -> String {
        return URL(fileURLWithPath: self).deletingPathExtension().lastPathComponent
    }
    
    func fileExtension() -> String {
        return URL(fileURLWithPath: self).pathExtension
    }
    
    public func toFloat() -> Float? {
        return Float.init(self)
    }
    
    public func toDouble() -> Double? {
        return Double.init(self)
    }
}

