//
//  CollectionViewMenuCell.swift
//  CaptureIT
//
//  Created by Deepak jaswal on 21/09/20.
//  Copyright © 2020 Deepak jaswal. All rights reserved.
//

import UIKit

class CollectionViewMenuCell: UICollectionViewCell {

    @IBOutlet weak var imgvw_Menu: UIImageView!
    @IBOutlet weak var filterName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
