//
//  ViewController.swift
//  CaptureIT
//
//  Created by Deepak jaswal on 21/09/20.
//  Copyright © 2020 Deepak jaswal. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import CoreImage
import AVKit


class ViewController: BaseController {
    
    @IBOutlet weak var videoView: VideoView!
    @IBOutlet weak var collectionViewMenu: UICollectionView!
    @IBOutlet weak var collectionViewOptions: UICollectionView!
    
    @IBOutlet weak var trimmerView: TrimmerView!
    
    @IBOutlet weak var mergeView: UIView!
    @IBOutlet weak var btnMergeVideo1: UIButton!
    @IBOutlet weak var btnMergeVideo2: UIButton!
    var selectedMergeVideoButton: Int = 0
    var assetDictForMerging = [Int: AVAsset]()
    
    @IBOutlet weak var stickerView: UIView!
    @IBOutlet weak var imgSticker: UIImageView!
    
    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var txtfieldVideo: UITextField!
    @IBOutlet weak var lblTextForVideo: UILabel!
    
    
    @IBOutlet weak var adjustmentView: UIView!
    @IBOutlet weak var saturationSlider: UISlider!
    @IBOutlet weak var collectionViewAdjustmentOptions: UICollectionView!
    
    var selectedAdjustmentOption: Int = 0
    
    var videoPicker: VideoPicker!
    var thumbnailImage = UIImage()
    var thumbnailImages: [UIImage] = []
    var selectedEditorOption: Int = 0
    var strSelectedSpeed = ""
    var selectedTransitionType = -1
    
    var playerController = AVPlayerViewController()
    
    var playbackTimeCheckerTimer: Timer?
    
    var selectedAudioFileURL = URL(string: "")
    var mergesliderminimumValue : Double = 0.0
    var mergeslidermaximumValue : Double = 0.0
    
    var videoTotalsec = 0.0
    var audioTotalsec = 0.0
    
    // Array Declartion
    //  var menuItems = ["filterW","cropW","audiomergeW","speedW","textW","stickerW", "videomergeW", "transitionW"]
    var menuItems = ["Filters", "Speed", "Trimming", "Merge", "Stickers", "Text", "Transition", "Audio", "Adjustment"]
    var speedOptions = ["0.10","0.25", "0.50", "0.75", "1.0", "1.25", "1.5", "2.0", "3.0", "4.0", "5.0"]
    var stickersPack = ["001","002", "003", "004", "005", "006", "007", "008", "009", "010", "011", "012", "013", "014", "015"]
    
    var transitionItems = ["Right to Left","Left to Right","Top to Bottom","Bottom to Top", "Lefttop to Rightbottom","Rightbottom to Lefttop", "Fade in/out"]
    
    var adjustmentItems = ["Saturation", "Brightness", "Contrast", "Warmth", "Noise Reduct", "Vignette", "Sharpness", "Exposure", "Hightlight"]
    
    private let myFilters: [String] = [
        
        "CIPhotoEffectInstant",
        "CIPhotoEffectNoir",
        "CIGaussianBlur",
        "CIPhotoEffectChrome",
        "CIPhotoEffectTonal"
    ]
    
    var applyFilter = String() {
        didSet {
            updateFilterOnVideo()
        }
    }
    
    var showTextForVideo: Bool = true {
        didSet {
            self.lblTextForVideo.isHidden = showTextForVideo
            self.textView.isHidden = showTextForVideo
        }
    }
    
    var context: CIContext!
    var currentFilter: CIFilter!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUPVideoView()
        collectionViewMenu.delegate = self
        collectionViewMenu.dataSource = self
        collectionViewOptions.delegate = self
        collectionViewOptions.dataSource = self
        collectionViewAdjustmentOptions.delegate = self
        collectionViewAdjustmentOptions.dataSource = self
        
        //Collection view Cell NIB Identifier
        collectionViewMenu.register(UINib(nibName: "CollectionViewMenuCell", bundle: Bundle.main), forCellWithReuseIdentifier: "CollectionViewMenuCell")
        
        collectionViewAdjustmentOptions.register(UINib(nibName: "CollectionViewAdjustmentOptionsCell", bundle: Bundle.main), forCellWithReuseIdentifier: "CollectionViewAdjustmentOptionsCell")
        
        collectionViewMenu.tag = 1
        collectionViewMenu.reloadData()
        
        context = CIContext()
        txtfieldVideo.delegate = self
    }
    
    @IBAction func videoPickerButtonTouched(_ sender: UIButton) {
        self.videoPicker = VideoPicker(presentationController: self, delegate: self)
        self.videoPicker.present(from: sender)
    }
    
    @IBAction func mergeVideo1Action(_ sender: UIButton) {
        self.selectedMergeVideoButton = 0
        self.videoPicker = VideoPicker(presentationController: self, delegate: self)
        self.videoPicker.present(from: sender)
    }
    @IBAction func mergeVideo2Action(_ sender: UIButton) {
        self.selectedMergeVideoButton = 1
        self.videoPicker = VideoPicker(presentationController: self, delegate: self)
        self.videoPicker.present(from: sender)
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        print(self.saturationSlider.value)
        switch self.selectedAdjustmentOption {
        case 1:
            //Brightness
            videoView.brightness = Int(sender.value)
        case 2:
            //Contrast
            videoView.contrast = Int(sender.value)
        case 3:
            //Warm
            if Int(sender.value) <= 500 {
            //warm
                videoView.warm = Int(sender.value)
            } else {
            //Cold
                videoView.cold = Int(sender.value)
            }
        case 4:
            // Noise Reduct
            videoView.noiseReduct = Int(sender.value)
        case 5:
            // vignette
            videoView.vignette = Int(sender.value)
        case 6:
            //Sharpness
            videoView.sharpness = Int(sender.value)
        case 7:
            // Exposure
            videoView.exposure = Int(sender.value)
        case 8:
            //Hightlight
            videoView.hightlight = Int(sender.value)
        default:
            //Saturation
            videoView.saturation = Int(sender.value)
        }
        
        self.videoView.player?.pause()
        self.videoView.player?.play()
    }
}

extension ViewController {
    //MARK: ==:> Video View SetUp
    
    func setUPVideoView() {
        self.videoView.contentMode = .scaleAspectFit
        self.videoView.player?.isMuted = true
        self.videoView.repeat = .once
    }
    fileprivate func updateFilterOnVideo() {
        videoView.filters = applyFilter
        self.videoView.player?.pause()
        self.videoView.player?.play()
        //        self.videoView.player?.pause()
        
        /*   self.videoView.player?.pause()
         
         videoView.addfiltertoVideo(strfiltername: applyFilter, success: { (url) in
         DispatchQueue.main.async {
         
         self.videoView.url = url
         
         self.videoView.player?.play()
         //                self.addVideoPlayer(videoUrl: url, to: self.videoView)
         }
         }) { (error) in
         DispatchQueue.main.async {
         //                OptiToast.showNegativeMessage(message: error ?? "")
         //                self.progressvw_back.isHidden = true
         }
         }*/
    }
    
    //MARK: Video Play Action
    func addVideoPlayer(videoUrl: URL, to view: UIView) {
        self.videoView.player = AVPlayer(url: videoUrl)
        playerController.player = self.videoView.player
        self.addChild(playerController)
        view.addSubview(playerController.view)
        playerController.view.frame = view.bounds
        playerController.showsPlaybackControls = true
        self.videoView.player?.play()
    }
    
    fileprivate func saveVideoToDocuments() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Video Editor", message: "Save current changes on Video !!", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.default, handler: { action in
                if let videourl = self.videoView.url {
                    let getalbum = UserDefaults.standard.bool(forKey: "AlbumCreated")
                    if getalbum {
                        self.save(videoUrl: videourl, toAlbum: "Video Editor", completionHandler: { (saved, error) in
                            DispatchQueue.main.async {
                                if saved {
                                    let saveBarBtnItm = UIBarButtonItem(title: "", style: .done, target: self, action: nil)
                                    self.navigationItem.rightBarButtonItem  = saveBarBtnItm
                                    //  OptiToast.showPositiveMessage(message: OptiConstant().videosaved)
                                    print("Video Saved!!")
                                }else {
                                    //  OptiToast.showNegativeMessage(message: error?.localizedDescription ?? "")
                                    print(error?.localizedDescription)
                                }
                            }
                        })
                    }else{
                        self.createAlbum(withTitle: "Video Editor", completionHandler: { (album) in
                            UserDefaults.standard.setValue(true, forKey: "AlbumCreated")
                            self.save(videoUrl: videourl, toAlbum: "Video Editor", completionHandler: { (saved, error) in
                                DispatchQueue.main.async {
                                    if saved {
                                        let saveBarBtnItm = UIBarButtonItem(title: "", style: .done, target: self, action: nil)
                                        self.navigationItem.rightBarButtonItem  = saveBarBtnItm
                                        //  OptiToast.showPositiveMessage(message: OptiConstant().videosaved)
                                        print("Video Saved")
                                    }else {
                                        //   OptiToast.showNegativeMessage(message: error?.localizedDescription ?? "")
                                        print(error?.localizedDescription)
                                    }
                                }
                            })
                        })
                    }
                }
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func saveActionforEditedVideo() {
        
        if self.mergeView.isHidden == false {
            var assetArray = [AVAsset]()
            for (_, asset)in self.assetDictForMerging {
                assetArray.append(asset)
            }
            self.videoView.mergeTwoVideosArry(arrayVideos: assetArray, success: { (url) in
                DispatchQueue.main.async {
                    let saveBarBtnItm = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.saveActionforEditedVideo))
                    self.navigationItem.rightBarButtonItem  = saveBarBtnItm
                    self.videoView.url = url
                    self.videoView.player?.play()
                    self.mergeView.isHidden = true
                    self.btnMergeVideo2.setBackgroundImage(nil, for: .normal)
                    self.btnMergeVideo1.setBackgroundImage(nil, for: .normal)
                    self.collectionViewMenu.isHidden = false
                    self.assetDictForMerging.removeAll()
                }
            }) { (error) in
                //                DispatchQueue.main.async {
                //                    OptiToast.showNegativeMessage(message: error ?? "")
                //                    self.progressvw_back.isHidden = true
                //                }
            }
        } else {
            self.videoView.addfiltertoVideo(strfiltername: self.myFilters[4], success: { (url) in
                DispatchQueue.main.async {
                    self.showSaveButton()
                    
                    self.saveVideoToDocuments()
                    //                self.progress_Vw.progress = 1.0
                    //                self.slctVideoUrl = url
                    //                self.addVideoPlayer(videoUrl: url, to: self.video_vw)
                    //                self.progressvw_back.isHidden = true
                }
            }) { (error) in
                DispatchQueue.main.async {
                    print(error)
                    //                OptiToast.showNegativeMessage(message: error ?? "")
                    //                self.progressvw_back.isHidden = true
                }
            }
        }
    }
    
    func downloadFile() {
        
        if let audioUrl = URL(string: "https://s3.amazonaws.com/kargopolov/kukushka.mp3") {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // lets create your destination file url
            let destinationUrl = documentsDirectoryURL.appendingPathComponent(audioUrl.lastPathComponent)
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager.default.fileExists(atPath: destinationUrl.path) {
                print("The file already exists at path")
                selectedAudioFileURL = destinationUrl
                self.mergeAudioVideo()
                // if the file doesn't exist
            } else {
                
                // you can use NSURLSession.sharedSession to download the data asynchronously
                URLSession.shared.downloadTask(with: audioUrl, completionHandler: { (location, response, error) -> Void in
                    guard let location = location, error == nil else { return }
                    do {
                        // after downloading your file you need to move it to your destination url
                        try FileManager.default.moveItem(at: location, to: destinationUrl)
                        print("File moved to documents folder")
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    }
                }).resume()
            }
        }
        
    }
}

extension ViewController: TrimmerViewDelegate {
    //MARK: ==:> Trimmer View SetUp
    func trimmerViewSetUp () {
        trimmerView.isHidden = false
        trimmerView.handleColor = UIColor.white
        trimmerView.mainColor = UIColor.darkGray
        
        DispatchQueue.main.async {
            let asset = AVAsset(url: self.videoView.url!)
            self.loadAsset(asset)
        }
    }
    func loadAsset(_ asset: AVAsset) {
        
        trimmerView.asset = asset
        trimmerView.delegate = self
        addVideoPlayer(with: asset, playerView: videoView)
    }
    private func addVideoPlayer(with asset: AVAsset, playerView: UIView) {
        let playerItem = AVPlayerItem(asset: asset)
        self.videoView.player = AVPlayer(playerItem: playerItem)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.itemDidFinishPlaying(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        self.videoView.player?.play()
        //        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        //        layer.backgroundColor = UIColor.white.cgColor
        //        layer.frame = CGRect(x: 0, y: 0, width: playerView.frame.width, height: playerView.frame.height)
        //        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        //        playerView.layer.sublayers?.forEach({$0.removeFromSuperlayer()})
        //        playerView.layer.addSublayer(layer)
    }
    
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        if let startTime = trimmerView.startTime {
            self.videoView.player?.seek(to: startTime)
        }
    }
    @objc func onPlaybackTimeChecker() {
        
        guard let startTime = trimmerView.startTime, let endTime = trimmerView.endTime/*, let player = player*/ else {
            return
        }
        
        let playBackTime = self.videoView.player?.currentTime()
        trimmerView.seek(to: playBackTime!)
        
        if playBackTime! >= endTime {
            self.videoView.player?.seek(to: startTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
            trimmerView.seek(to: startTime)
        }
    }
    
    func startPlaybackTimeChecker() {
        
        stopPlaybackTimeChecker()
        playbackTimeCheckerTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                                        selector:
            #selector(ViewController.onPlaybackTimeChecker), userInfo: nil, repeats: true)
    }
    func stopPlaybackTimeChecker() {
        
        playbackTimeCheckerTimer?.invalidate()
        playbackTimeCheckerTimer = nil
    }
    
    func positionBarStoppedMoving(_ playerTime: CMTime) {
        self.videoView.player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        self.videoView.player?.play()
        startPlaybackTimeChecker()
    }
    
    func didChangePositionBar(_ playerTime: CMTime) {
        stopPlaybackTimeChecker()
        self.videoView.player?.pause()
        self.videoView.player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        let duration = (trimmerView.endTime! - trimmerView.startTime!).seconds
        print(duration)
    }
}

extension ViewController: VideoPickerDelegate {
    //MARK: ==:> Video Picker Delegate
    
    fileprivate func getThumbnailImagesWithFilterEffects(_ url: URL) {
        if let thumbImg = self.getThumbnailImage(forUrl: url) {
            self.thumbnailImage = self.resizeImage(image: thumbImg, targetSize: CGSize(width: collectionViewMenu.frame.height, height: collectionViewMenu.frame.height - 30))
            self.thumbnailImages.removeAll()
            //  DispatchQueue.main.async {
            for index in  0..<self.myFilters.count {
                self.currentFilter = CIFilter(name: self.myFilters[index])
                let beginImage = CIImage(image: self.thumbnailImage)
                self.currentFilter.setValue(beginImage, forKey: kCIInputImageKey)
                
                self.applyProcessing()
            }
            self.collectionViewMenu.reloadData()
            // }
        }
    }
    
    fileprivate func showSaveButton() {
        let saveBarBtnItm = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(self.saveActionforEditedVideo))
        self.navigationItem.rightBarButtonItem  = saveBarBtnItm
    }
    
    fileprivate func updateViewAfterGettingVideo() {
        self.collectionViewOptions.isHidden = false
        showSaveButton()
    }
    
    func didSelect(url: URL?) {
        guard let url = url else {
            return
        }
        self.videoView.url = url
        
        if self.mergeView.isHidden == false {
            self.showSaveButton()
            self.getThumbnailImageForVideoMergeButtons(url: url)
        } else {
            self.updateViewAfterGettingVideo()
            self.getThumbnailImagesWithFilterEffects(url)
        }
        self.videoView.player?.play()
    }
    
    func getThumbnailImageForVideoMergeButtons(url: URL) {
        guard let thumbImg = self.getThumbnailImage(forUrl: url) else { return }
        let asset = AVAsset(url: url)
        self.assetDictForMerging[self.selectedMergeVideoButton] = asset
        if self.selectedMergeVideoButton == 0 {
            self.btnMergeVideo1.setBackgroundImage(self.resizeImage(image: thumbImg, targetSize: CGSize(width: self.btnMergeVideo1.frame.height, height: self.btnMergeVideo1.frame.height)), for: .normal)
        } else {
            self.btnMergeVideo2.setBackgroundImage(self.resizeImage(image: thumbImg, targetSize: CGSize(width: self.btnMergeVideo2.frame.height, height: self.btnMergeVideo2.frame.height)), for: .normal)
        }
    }
    
    func applyProcessing() {
        guard let image = currentFilter.outputImage else { return }
        //  currentFilter.setValue(10.0, forKey: kCIInputIntensityKey)
        
        if let cgimg = context.createCGImage(image, from: image.extent) {
            let processedImage = UIImage(cgImage: cgimg)
            self.thumbnailImages.append(processedImage)
        }
    }
}

extension ViewController: UICollectionViewDataSource {
    //MARK: ==:> CollectionView Delegate & Data Source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard collectionView == self.collectionViewOptions else {
            guard collectionView == self.collectionViewAdjustmentOptions else {
                switch self.selectedEditorOption {
                case 1: return self.speedOptions.count
                case 4: return self.stickersPack.count
                case 6: return self.transitionItems.count
                default: return self.thumbnailImages.count
                }
            }
            return self.adjustmentItems.count
        }
        return menuItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard collectionView == self.collectionViewOptions else {
            guard collectionView == self.collectionViewAdjustmentOptions else {
                switch self.selectedEditorOption {
                case 1:
                    let cell: CollectionViewMenuCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewMenuCell", for: indexPath) as! CollectionViewMenuCell
                    //        cell.imgvw_Menu.image = UIImage(named:menuItems[indexPath.row])
                    
                    cell.imgvw_Menu.image = nil
                    cell.filterName.text = self.speedOptions[indexPath.row]
                    
                    return cell
                    
                case 4:
                    let cell: CollectionViewMenuCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewMenuCell", for: indexPath) as! CollectionViewMenuCell
                    //        cell.imgvw_Menu.image = UIImage(named:menuItems[indexPath.row])
                    
                    //                cell.imgvw_Menu.image = UIImage(named: self.stickersPack[indexPath.row])
                    cell.imgvw_Menu.image = UIImage(named: "sticker\(indexPath.row)")
                    cell.imgvw_Menu.contentMode = .scaleToFill
                    cell.filterName.text = ""
                    
                    return cell
                    
                case 6:
                    let cell: CollectionViewMenuCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewMenuCell", for: indexPath) as! CollectionViewMenuCell
                    //        cell.imgvw_Menu.image = UIImage(named:menuItems[indexPath.row])
                    
                    cell.imgvw_Menu.image = nil
                    cell.filterName.text = self.transitionItems[indexPath.row]
                    if self.selectedTransitionType == indexPath.row {
                        cell.backgroundColor = .white
                        cell.filterName.textColor = .red
                    } else {
                        cell.backgroundColor = .clear
                        cell.filterName.textColor = .yellow
                    }
                    return cell
                    
                default:
                    let cell: CollectionViewMenuCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewMenuCell", for: indexPath) as! CollectionViewMenuCell
                    //        cell.imgvw_Menu.image = UIImage(named:menuItems[indexPath.row])
                    
                    cell.imgvw_Menu.image = self.thumbnailImages[indexPath.row]
                    cell.filterName.text = "\(indexPath.row + 1)"
                    
                    return cell
                }
            }
            let cell: CollectionViewAdjustmentOptionsCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewAdjustmentOptionsCell", for: indexPath) as! CollectionViewAdjustmentOptionsCell
            
            if indexPath.row == self.selectedAdjustmentOption {
                cell.lbl.textColor = .white
            } else {
                cell.lbl.textColor = .yellow
            }
            cell.lbl.text = adjustmentItems[indexPath.row]
            
            return cell
        }
        
        return mainEditorOptions(collectionView, indexPath)
        
    }
    
    
    // Main Editor Options menu ==>
    fileprivate func mainEditorOptions(_ collectionView: UICollectionView, _ indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CollectionViewOptionsCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewOptionsCell", for: indexPath) as! CollectionViewOptionsCell
        
        if indexPath.row == self.selectedEditorOption {
            cell.lbl.textColor = .white
        } else {
            cell.lbl.textColor = .black
        }
        cell.lbl.text = menuItems[indexPath.row]
        
        return cell
    }
}


extension ViewController: UICollectionViewDelegate {
    //MARK: ==:> CollectionView Delegate
    
    fileprivate func handleVideoSpeedOnClick(_ indexPath: IndexPath) {
        self.strSelectedSpeed = self.speedOptions[indexPath.row]
        
        let num = strSelectedSpeed.toDouble()
        self.videoView.videoScaleAssetSpeed(fromURL: self.videoView.url!, by: num ?? 1.0, success: { (url) in
            DispatchQueue.main.async {
                self.showSaveButton()
                //                        self.progress_Vw.progress = 1.0
                //  self.addVideoPlayer(videoUrl: url, to: self.video_vw)
                self.videoView.url = url
                self.videoView.player?.pause()
                self.videoView.player?.play()
                //                        self.progressvw_back.isHidden = true
            }
        }) { (error) in
            DispatchQueue.main.async {
                print(error ?? "bug here!!")
                //                        OptiToast.showNegativeMessage(message: error ?? "")
                //                        self.progressvw_back.isHidden = true
            }
        }
    }
    
    fileprivate func showTransitionOnVideo(_ indexPath: IndexPath) {
        self.selectedTransitionType = indexPath.row
        self.collectionViewMenu.reloadData()
        DispatchQueue.main.async {
            self.videoView.player?.pause()
        }
        self.videoView.transitionAnimation(videoUrl: self.videoView.url!, animation: true, type: selectedTransitionType, playerSize: self.videoView.frame, success: { (url) in
            DispatchQueue.main.async {
                let saveBarBtnItm = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(self.saveActionforEditedVideo))
                self.navigationItem.rightBarButtonItem  = saveBarBtnItm
                self.videoView.url = url
                self.videoView.player?.pause()
                self.videoView.player?.play()
                //   self.progress_Vw.progress = 1.0
                //   self.slctVideoUrl = url
                //   self.addVideoPlayer(videoUrl: url, to: self.video_vw)
                //    self.progressvw_back.isHidden = true
            }
        }) { (error) in
            // DispatchQueue.main.async {
            //   OptiToast.showNegativeMessage(message: error ?? "")
            //   self.progressvw_back.isHidden = true
            
            // }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard collectionView == self.collectionViewOptions else {
            guard collectionView == self.collectionViewAdjustmentOptions else {
                switch self.selectedEditorOption {
                    
                case 1:
                    handleVideoSpeedOnClick(indexPath)
                    break
                case 4:
                    self.stickerView.isHidden = false
                    self.imgSticker.image = UIImage(named: "sticker\(indexPath.row)")
                    break
                case 6:
                    showTransitionOnVideo(indexPath)
                    break
                    
                default:
                    applyFilter = myFilters[indexPath.row]
                    /*    self.save(videoUrl: self.videoView.url!, toAlbum: "Video Editor", completionHandler: { (saved, error) in
                     DispatchQueue.main.async {
                     print(saved)
                     //                if saved {
                     //                    let saveBarBtnItm = UIBarButtonItem(title: "", style: .done, target: self, action: nil)
                     //                    self.navigationItem.rightBarButtonItem  = saveBarBtnItm
                     ////                    OptiToast.showPositiveMessage(message: OptiConstant().videosaved)
                     //                }else {
                     ////                    OptiToast.showNegativeMessage(message: error?.localizedDescription ?? "")
                     //                }
                     }
                     })*/
                }
                return
            }
            self.selectedAdjustmentOption = indexPath.row
            self.saturationSlider.value = 0.0
            
            switch self.selectedAdjustmentOption {
            case 1:
                //Brightness
                self.saturationSlider.maximumValue = 5.0
            case 2:
                //Contrast
                self.saturationSlider.maximumValue = 50.0
            case 3:
                //Warm
                self.saturationSlider.maximumValue = 1000.0
                self.saturationSlider.value = 500.0
            case 4:
                // Noise Reduct
                self.saturationSlider.maximumValue = 50.0
            case 5:
                // vignette
                self.saturationSlider.maximumValue = 50.0
            case 6:
                //Sharpness
                self.saturationSlider.maximumValue = 50.0
            case 7:
                //Exposure
                self.saturationSlider.maximumValue = 15.0
            case 8:
                //Hightlight
                self.saturationSlider.maximumValue = 50.0
            default:
                //Saturation
                self.saturationSlider.maximumValue = 100.0
            }
            
            self.collectionViewAdjustmentOptions.reloadData()
            return
        }
        
        // Main Editor Options
        guard self.selectedEditorOption != indexPath.row else { return }
        self.selectedEditorOption = indexPath.row
        self.collectionViewOptions.reloadData()
        if indexPath.row == 2 {
            // trimming Video
            self.collectionViewMenu.isHidden = true
            self.mergeView.isHidden = true
            self.trimmerViewSetUp()
        } else if indexPath.row == 3 {
            // Merge video
            self.collectionViewMenu.isHidden = true
            self.trimmerView.isHidden = true
            self.mergeVideoSetUp()
        } else if indexPath.row == 7 {
            self.showAudioSelectionOption()
        } else {
            self.trimmerView.isHidden = true
            self.mergeView.isHidden = true
            self.collectionViewMenu.isHidden = false
            self.collectionViewMenu.reloadData()
        }
        
        self.adjustmentView.isHidden = (indexPath.row == 8) ? false : true
        self.showTextForVideo = (indexPath.row == 5) ? false : true
    }
}


extension ViewController : UICollectionViewDelegateFlowLayout {
    //MARK: ==:> CollectionView Layout Delegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard collectionView == self.collectionViewOptions else {
            
            guard collectionView == self.collectionViewAdjustmentOptions else {
                let orientation = UIApplication.shared.statusBarOrientation
                if(orientation == .landscapeLeft || orientation == .landscapeRight) {
                    return CGSize(width: collectionViewMenu.frame.width / 7.0, height: collectionViewMenu.frame.height - 10)
                } else {
                    return CGSize(width: collectionViewMenu.frame.width / 7.0, height: collectionViewMenu.frame.height - 10)
                }
            }
            return CGSize(width: 20, height: 100)
        }
        return CGSize(width: (collectionViewMenu.frame.width)/2, height: collectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
        
        
    }
}

extension ViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let finalString: String = "\(textField.text ?? "")\(string)"
        self.lblTextForVideo.text = finalString
        
        return true
    }
}

extension ViewController {
    //MARK: ==:> Merge Video
    func mergeVideoSetUp () {
        self.mergeView.isHidden = false
        self.getThumbnailImageForVideoMergeButtons(url: self.videoView.url!)
    }
    
}

extension ViewController {
    //MARK: ==:> Add Audio
    func showAudioSelectionOption() {
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Download From", message: "https://s3.amazonaws.com/kargopolov/kukushka.mp3", preferredStyle: .actionSheet)
        
        let saveActionButton = UIAlertAction(title: "Select", style: .default)
        { _ in
            self.downloadFile()
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .default)
        { _ in
            print("cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    func mergeAudioVideo() {
        
        self.videoView.trimAudio(sourceURL: self.selectedAudioFileURL!, startTime: mergesliderminimumValue, stopTime: mergeslidermaximumValue, success: { (audioUrl) in
            let asset = AVAsset(url: audioUrl)
            let audiosec = CMTimeGetSeconds(asset.duration)
            
            let assetV = AVAsset(url: self.videoView.url!)
            let duration = assetV.duration
            let durationTime = CMTimeGetSeconds(duration)
            let videoTotalsec = durationTime
            
            if videoTotalsec <= audiosec {
                DispatchQueue.main.async {
                    // self.progressvw_back.isHidden = false
                    //  self.progress_Vw.progress = 0.1
                    //  self.setTimer()
                    //  self.merge_Musicbacvw.isHidden = true
                    //  self.mergeView.isHidden = true
                    if  let videourl = self.videoView.url  {
                        self.videoView.mergeVideoWithAudio(videoUrl: videourl, audioUrl: audioUrl, success: { (url) in
                            DispatchQueue.main.async {
                                let saveBarBtnItm = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(self.saveActionforEditedVideo))
                                self.navigationItem.rightBarButtonItem  = saveBarBtnItm
                                self.videoView.url = url
                                self.videoView.player?.pause()
                                self.videoView.player?.play()
                                //  self.progress_Vw.progress = 1.0
                                //  self.slctVideoUrl = url
                                //   self.addVideoPlayer(videoUrl: url, to: self.video_vw)
                                //    self.progressvw_back.isHidden = true
                            }
                        }) { (error) in
                            DispatchQueue.main.async {
                                //   OptiToast.showNegativeMessage(message: error ?? "")
                                //   self.progressvw_back.isHidden = true
                            }
                        }
                    }
                }
            }else{
                DispatchQueue.main.async {
                    //  OptiToast.showNegativeMessage(message: OptiConstant().cropaudioduration)
                    //   self.progressvw_back.isHidden = true
                }
            }
            
        }) { (error) in
            DispatchQueue.main.async {
                // OptiToast.showNegativeMessage(message: error ?? "")
                //  self.progressvw_back.isHidden = true
            }
        }
    }
}
